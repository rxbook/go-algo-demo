// 使用链表实现队列
package main

import (
	"fmt"
)

type ListNode struct {
	val  interface{}
	next *ListNode
}

type LinkedListQueue struct {
	head   *ListNode
	tail   *ListNode
	length int
}

func NewLinkedListQueue() *LinkedListQueue {
	return &LinkedListQueue{nil, nil, 0}
}

func (this *LinkedListQueue) Enqueue(v interface{}) {
	node := &ListNode{v, nil}
	if nil == this.tail {
		this.tail = node
		this.head = node
	} else {
		this.tail.next = node
		this.tail = node
	}
	this.length++
}

func (this *LinkedListQueue) Dequeue() interface{} {
	if this.head == nil {
		return nil
	}
	v := this.head.val
	this.head = this.head.next
	this.length--
	return v
}

func (this *LinkedListQueue) String() string {
	if this.head == nil {
		return "empty queue"
	}
	result := ""
	for cur := this.head; cur != nil; cur = cur.next {
		result += fmt.Sprintf("%v ", cur.val)
	}
	return result
}

func main() {
	queue := NewLinkedListQueue()

	//入队6个元素
	queue.Enqueue(1)
	queue.Enqueue(2)
	queue.Enqueue(3)
	queue.Enqueue(4)
	queue.Enqueue(5)
	queue.Enqueue(6)
	fmt.Println(queue) //1 2 3 4 5 6

	//出队1个元素
	queue.Dequeue()
	fmt.Println(queue) // 2 3 4 5 6

	//出队3个元素
	queue.Dequeue()
	queue.Dequeue()
	queue.Dequeue()
	fmt.Println(queue) //5 6
}
