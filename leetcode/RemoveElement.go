// 力扣27. 移除元素
// https://leetcode.cn/problems/remove-element/
package main

import "fmt"

func removeElement(nums []int, val int) (int, []int) {
	ret := 0
	//把nums[0:ret]当做新数组，把nums数组中不等于val的元素覆盖的插入到里面
	for i := 0; i < len(nums); i++ {
		if nums[i] != val {
			nums[ret] = nums[i]
			ret++
		}
	}

	// 返回新的数组中已经重新赋值过的长度
	return ret, nums[0:ret]
}

func main() {
	fmt.Println(removeElement([]int{3, 2, 2, 3}, 3))             //2 [2 2]
	fmt.Println(removeElement([]int{0, 1, 2, 2, 3, 0, 4, 2}, 2)) //5 [0 1 3 0 4]
}
