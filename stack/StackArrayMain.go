package main

import "fmt"

// go run StackArray.go StackArrayMain.go
func main() {
	s := NewArrayStack()
	// 向栈中插入元素
	s.Push(1)
	s.Push(2)
	s.Push(3)
	s.Print() //3 2 1

	// 获取当前栈顶的元素
	fmt.Println(s.Top()) //3

	// 从栈中弹出元素
	fmt.Println(s.Pop()) //3
	fmt.Println(s.Pop()) //2
	fmt.Println(s.Pop()) //1
	fmt.Println(s.Pop()) //<nil>
	s.Print()            //empty statck
}
