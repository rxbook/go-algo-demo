// 基于数组实现的栈
package main

import (
	"fmt"
)

type StackArray struct {
	data []interface{} //栈里面的数据
	top  int           //栈顶指针位置
}

// 初始化一个栈
func NewArrayStack() *StackArray {
	return &StackArray{
		data: make([]interface{}, 0, 32),
		top:  -1,
	}
}

// 判断栈是否为空
func (this *StackArray) IsEmpty() bool {
	if this.top < 0 {
		return true
	}
	return false
}

// 获取栈顶指针
func (this *StackArray) GetTopValue() int {
	return this.top
}

// 向栈中插入元素
func (this *StackArray) Push(v interface{}) {
	if this.top < 0 {
		this.top = 0
	} else {
		this.top += 1
	}

	if this.top > len(this.data)-1 {
		this.data = append(this.data, v)
	} else {
		this.data[this.top] = v
	}
}

// 从栈中弹出元素
func (this *StackArray) Pop() interface{} {
	if this.IsEmpty() {
		return nil
	}
	v := this.data[this.top]
	this.top -= 1
	return v
}

// 获取当前栈顶的元素
func (this *StackArray) Top() interface{} {
	if this.IsEmpty() {
		return nil
	}
	return this.data[this.top]
}

// 清空栈
func (this *StackArray) Flush() {
	this.top = -1
}

// 打印输出
func (this *StackArray) Print() {
	if this.IsEmpty() {
		fmt.Println("empty statck")
	} else {
		for i := this.top; i >= 0; i-- {
			fmt.Print(this.data[i], " ")
		}
	}
	fmt.Println()
}
