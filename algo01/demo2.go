// https://blog.csdn.net/rxbook/article/details/130854684
package main

import (
	"fmt"
	"strconv"
)

// 切片的动态扩容是二倍扩容
func appendSlice() {
	sli := []int{1, 2, 3}
	fmt.Printf("slice=%v len=%d cap=%d\n", sli, len(sli), cap(sli)) //slice=[1 2 3] len=3 cap=3
	sli = append(sli, 4)
	fmt.Printf("slice=%v len=%d cap=%d\n", sli, len(sli), cap(sli)) //slice=[1 2 3 4] len=4 cap=6
	sli = append(sli, 5)
	fmt.Printf("slice=%v len=%d cap=%d\n", sli, len(sli), cap(sli)) //slice=[1 2 3 4 5] len=5 cap=6
	sli = append(sli, 6)
	fmt.Printf("slice=%v len=%d cap=%d\n", sli, len(sli), cap(sli)) //slice=[1 2 3 4 5 6] len=6 cap=6
	sli = append(sli, 7)
	fmt.Printf("slice=%v len=%d cap=%d\n", sli, len(sli), cap(sli)) //slice=[1 2 3 4 5 6 7] len=7 cap=12
}

// 力扣9. 回文数 https://leetcode.cn/problems/palindrome-number/
// 思路 1：判断 x 是否为负数，如果是负数直接返回；反转 x , 如果反转之后的值与原来的值不同直接返回 false；如果不为负数，同时与反转后的值相等则返回 true。
// 时间复杂度: O(N)，空间复杂度: O(N)
func isPalindrome1(x int) bool {
	if x < 0 { // 排除小于0的数
		return false
	}
	xStr := strconv.Itoa(x)
	xStrReverse := make([]rune, 0)
	for i, _ := range xStr {
		xStrReverse = append(xStrReverse, rune(xStr[len(xStr)-1-i]))
	}
	for i := 0; i < len(xStr); i += 1 { // 通过字符串进行反转，对比数字是否相等就行
		if rune(xStr[i]) != xStrReverse[i] {
			return false
		}
	}
	return true
}

// 思路2：直接把整数反转过来，与原来的值比较即可。时间复杂度: O(1)，空间复杂度: O(1)
func isPalindrome2(x int) bool {
	// 负数肯定不是palindrome
	// 如果一个数字是一个正数，并且能被10整除，那它肯定也不是palindrome，因为首位肯定不是 0
	if x < 0 || (x != 0 && x%10 == 0) {
		return false
	}
	rev, y := 0, x
	for x > 0 {
		rev = rev*10 + x%10
		x /= 10
	}
	return y == rev
}

func main() {
	appendSlice()
	fmt.Println(isPalindrome1(12321)) //true
	fmt.Println(isPalindrome1(1212))  //false

	fmt.Println(isPalindrome2(12321)) //true
	fmt.Println(isPalindrome2(1212))  //false
}
