// 双向链表
package main

import "fmt"

type ListNode struct {
	Value int
	Prev  *ListNode
	Next  *ListNode
}

type DoubleList struct {
	Head   *ListNode
	Tail   *ListNode
	Length int
}

// 给链表中追加元素
func (list *DoubleList) Append(x int) {
	node := &ListNode{Value: x}
	tail := list.Tail
	if tail == nil {
		list.Head = node
		list.Tail = node
	} else {
		tail.Next = node
		node.Prev = tail
		list.Tail = node
	}
	list.Length += 1
}

// 获取链表中的元素
func (list *DoubleList) Get(idx int) *ListNode {
	if list.Length <= idx {
		return nil
	}
	curr := list.Head
	for i := 0; i < idx; i++ {
		curr = curr.Next
	}
	return curr
}

// 在链表中指定元素后面插入新元素
func (list *DoubleList) InsertAfter(x int, prevNode *ListNode) {
	node := &ListNode{Value: x}
	if prevNode.Next == nil {
		prevNode.Next = node
		node.Prev = prevNode
	} else {
		nextNode := prevNode.Next
		nextNode.Prev = node
		node.Next = nextNode
		prevNode.Next = node
		node.Prev = prevNode
	}
}

// 遍历输出链表的元素
func (list *DoubleList) foreach() {
	curr := list.Head
	for curr != nil {
		fmt.Printf("%d ", curr.Value)
		curr = curr.Next
	}
	fmt.Println()
}

func main() {
	list := new(DoubleList)
	list.Append(1)
	list.Append(2)
	list.Append(3)
	list.Append(4)
	list.Append(5)
	list.foreach() //1 2 3 4 5

	node := list.Get(3)     //获取第3个元素的位置信息(从0开始)
	fmt.Println(node.Value) //4

	list.InsertAfter(9, node) //在第3个位置插入一个新元素9
	list.foreach()            //1 2 3 4 9 5
}
